# BioDataReader

BioDataReader is a flexible python framework for reading and converting bio(-chemical) data, like DNA or protein sequences originating from web-servers, including metadata and semantics.